<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/viviendaDAO.php";
class vivienda{
    private $idVivienda;
    private $direccion;
    private $telefono;
    private $barrio;
    private $conexion;
    private $viviendaDAO;
    
    public function getIdVivienda(){
        return $this -> idVivienda;
    }
    
    public function getDireccion(){
        return $this -> direccion;
    }
    
    public function getTelefono(){
        return $this -> telefono;
    }
    
    public function getBarrio(){
        return $this -> barrio;
    }
    
    public function vivienda($idVivienda = "",$direccion = "", $telefono = "", $barrio = ""){
        $this -> idVivienda= $idVivienda;
        $this -> direccion= $direccion;
        $this -> telefono = $telefono;
        $this -> barrio = $barrio;
        $this -> conexion = new Conexion();
        $this -> viviendaDAO= new viviendaDAO($this -> idVivienda, $this -> direccion, $this -> $telefono, $this -> barrio);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> viviendaDAO -> insertar());
        $this -> conexion -> cerrar();
    
    
    }  
    
    
   
    
}

?>